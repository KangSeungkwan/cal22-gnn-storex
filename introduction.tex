Graph neural networks (GNNs) have emerged as a standard method for learning data on large graphs. GNNs generalize conventional deep neural networks (DNNs) to capture non-Euclidean information by incorporating various features into geometric data \cite{zhu2020graph}. Recently, both academia and industry have demonstrated the success of GNNs in various applications such as recommendation systems by highlighting their high accuracy and flexibility compared to existing DNNs \cite{zhou2020graph}.

GNNs can be represented by a combination of conventional graph processing and DNN data processing methods. GNNs accumulate feature vectors of many vertices into a smaller subspace, called \emph{aggregation}. They then project the accumulated results on a subspace through a learned project matrix, referred to as \emph{transformation}. As aggregation requires traversing a given graph, it uses existing graph processing approaches such as message passing.
This unique property of GNNs makes the aggregation sparse in computation and exhibits many irregular memory accesses. On the other hand, the transformation is equivalent to a batched, fully connected layer in DNNs, which densely computes the inputs, similar to traditional deep learning \cite{lai2020policy}.
Owing to these distinctive behaviors of GNNs, hardware research to accelerate GNNs is immensely experiencing a resurgence \cite{yan2020hygcn,geng2021gcn}. In parallel, diverse software studies revamp the DNN frameworks to support various GNN models \cite{you2020design}.
For example, heterogeneous accelerators \cite{yan2020hygcn} employ many vector processors and systolic arrays to shorten the latency of aggregation and transformation.
Emerging software libraries (e.g., DGL \cite{wang2019dgl} and PyG \cite{fey2019fast}) allow GPUs to perform GNNs without exhibiting irregular memory access patterns by employing new types of matrix multiplication such as SDMM and SpMM \cite{canny2013big}.

While these efforts of the prior studies can shorten the GNN inference latency, user-level inference services, unfortunately, still suffer from low performance.
This is because most hardware and software frameworks focus on accelerating GNN algorithms themselves, but the inference services in practice need to handle a set of time-consuming tasks before running the actual GNN algorithms.
Specifically, we observe that the latency of GNN preprocessing is higher than that of the aggregation and transformation by 15.4$\times$, on average (\textsection \ref{sec:background}).

We propose a novel hardware design, \emph{PreGNN}, that accelerates the preprocessing tasks of GNNs from the beginning to the end, such that it can take the preprocessing overhead off the critical path in GNNs. In our design, PreGNN consists of three hardware acceleration components: i) \emph{parallel graph generator} (gGen), ii) \emph{unique random sampler} (uSampler), and iii) \emph{near-interconnect gather} (iGather).
gGen automatically sorts an edge array based on destination information and converts it into a sparse graph matrix, called \emph{compressed sparse row} (CSR), in parallel.
uSampler then randomly selects a given number of neighbor nodes from the vertices associated with the target.
To alleviate the preprocessing overhead through hardware automation, we propose two simple but efficient logic modules that do vertex sorting and data shuffling in a single cycle (\textsection \ref{sec:design}).
iGather finally composes a subgraph and an embedding table by referring to the sampled nodes for GPUs.

The main contribution of this work can be summarized as follow:
\begin{itemize}
  \item \emph{End-to-end characteristic analysis.}
  We analyze the GNN end-to-end latency and show that preprocessing is on the critical path of GNN inference services rather than the GNN model computation time itself (93.9\% of the total GNN service time, on average).

  \item \noindent \emph{Preprocessing automation.}
  PreGNN removes the preprocessing overhead by accelerating different GNN tasks over hardware. Our approach can be applied to diverse GNN models \cite{kipf2017semi, wang2019ngcf, zhou2020graph}.


  \item \emph{Practical hardware design.}
  This work demonstrates that graph generation and neighbor sampling can be accelerated through common hardware logic, which makes PreGNN practical to implement in diverse acceleration domains.
\end{itemize}

We prototype PreGNN in real hardware containing a customized add-in-card type coprocessor with 16$nm$ FPGA and 64GB DDR4 DRAM.
Our evaluation results show that PreGNN can shorten the end-to-end latency of GNN inferences by 10.7$\times$ while reducing the energy by 3.3$\times$, on average, compared to a GPU-only system.

\begin{figure}[]
  \centering
  \includegraphics[width=1\linewidth]{images/preprocessing.eps}
  \vspace{-21pt}
  \caption{End-to-end procedure of a GNN inference.} \label{fig:preprocessing}
  \vspace{-18pt}
\end{figure}
