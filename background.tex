\noindent \textbf{GNN preprocessing.}
Figure \ref{fig:preprocessing} shows the whole procedure of a GNN inference service from the beginning to the end.
Most modern GNNs use SDDMM/SpMM \cite{canny2013big}, and their node sampling algorithm requires traversing an input graph in a vertex-centric manner.
Thus, they require converting raw-data graph (i.e., edge array) to a compact, memory-efficient graph matrix, called CSR.
Once this preprocessing task, called \emph{graph generation}, completes, GNNs sample multiple neighbor nodes of a given target vertex.
While several algorithms are proposed to make this \emph{node sampling} efficient, the $k$-hop neighbor sampling of GraphSAGE \cite{hamilton2017inductive} is the most popular algorithm and is widely used.

% \begin{figure}
%   \centering
%   \vspace{-2pt}
%   \begin{minipage}[b]{.39\linewidth}
%     \includegraphics[width=1\linewidth,bb=0 0 205 116]{images/break25.eps}
%     \vspace{-16pt}
%     \caption{Breakdown analysis.} \label{fig:breakdown}
%   \end{minipage}
%   \begin{minipage}[b]{.29\linewidth}
%     \setlength{\tabcolsep}{0.8pt}
%     \renewcommand{\arraystretch}{.9}
%     \resizebox{\linewidth}{!}{%
%       \small
%       \begin{tabular}{@{}lrrr@{}}
%         \toprule
%         \multicolumn{1}{c}{\textbf{Legend}} & \multicolumn{1}{c}{\textbf{Features}} & \multicolumn{1}{c}{\textbf{Vertices}} & \multicolumn{1}{c}{\textbf{Edges}} \\
%         \midrule
%         lastfmasia      &  128              &  7.6K             &  63.2K          \\
%         pubmedF         &  500              & 19.7K             & 108.3K          \\
%         citeseerF       &  602              &  4.2K             &  14.9K          \\
%         actor           &  932              &  7.6K             &  60.9K          \\
%         cora            & 1433              &  2.5K             &  12.6K          \\
%         dblpF           & 1639              & 17.7K             & 123.4K          \\
%         coramlF         & 2879              &  3.0K             &  19.3K          \\
%         citeseer        & 3703              &  2.1K             &   9.5K          \\
%         \bottomrule
%       \end{tabular}
%     }
%     \vspace{-5pt}
%     \captionof{table}{Datasets.} \label{tbl:dataset}
%   \end{minipage}
%   \begin{minipage}[b]{.3\linewidth}
%     \includegraphics[width=1\linewidth,bb=4 0 87 64]{images/over.eps}
%     \vspace{-20pt}
%     \caption{PreGNN.} \label{fig:over4}
%   \end{minipage}
%   \vspace{-28pt}
% \end{figure}
\begin{figure}
  \centering
  \includegraphics{images/motivation.eps}
  \begin{subfigure}{0\linewidth}
    \caption{}{}
    \label{tbl:dataset}
  \end{subfigure}
  \begin{subfigure}{0\linewidth}
    \caption{}
    \label{fig:breakdown}
  \end{subfigure}
  \begin{subfigure}{0\linewidth}
    \caption{}
    \label{fig:over4}
  \end{subfigure}
  \caption{Motivation and overview.}
\end{figure}

Specifically, it randomly selects $n$ numbers of per-hop nodes in a given graph by visiting all neighbors of the target in a range of $k$ hops.
The algorithm ensures that all the sampled nodes are unique and uniformly distributed to maintain high accuracy.
GNNs should then prepare appropriate data structures for a (sampled) subgraph and an embedding table as an input of their inference models.
Specifically, the subgraph and embeddings are gathered together in contiguous memory for efficient data transfer between the host and GPU.
Once the data are transferred, GPU can ``aggregate'' all the gathered features (per neighborhood) into a vector and ``transform'' the vector to an output embedding.
Although aggregation and transformation are the key functions of GNNs, we argue that the preprocessing tasks (i.e., graph generation, node sampling, and data preparation) are also essential and should be considered in the GNN service acceleration.

\noindent \textbf{Latency overhead and bottleneck analysis.}
Even though preprocessing is an integral part of the GNN services, it unfortunately takes much longer execution time than GNN's key functions.
To be precise, we analyze the end-to-end service latency of \emph{graph attention networks}, GAT \cite{ji2020graph}, which is the most popular GNN model built for recommendation systems.
We evaluate eight real-world graphs \cite{yang2016revisiting} on an acceleration system that employs a 1.8GHz 8-core CPU with 128GB DRAM and a high-performance GPU (RTX3090).
The important characteristics of our graph datasets are explained in Table \ref{tbl:dataset}.
Figure \ref{fig:breakdown} decomposes the end-to-end latency into execution time of graph generation (\texttt{Graph}), node sampling (\texttt{Sample}), data reconstruction (\texttt{Reconst}), and aggregation/transformation (\texttt{Infer}).
One can see from the figure that preprocessing accounts for 93.9\% of the total latency, making it the performance bottleneck in GNN inference services.
Since \texttt{Graph} requires sorting the input edge array in ascending order by destination vertex ID (\emph{VID}), it consumes many CPU cycles for VID comparisons. Before running GAT in GPU, \texttt{Sample}/\texttt{Reconst} also needs to reshape/move the graph data from/to different memory regions back and forth.
This, unfortunately, introduces many data copies and irregular memory accesses, thereby exhibiting long latency.
