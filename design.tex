\noindent \textbf{PreGNN overview.}
Figure \ref{fig:over4} shows a high-level view of PreGNN, designed to accelerate GNN preprocessing tasks.
As a coprocessing unit, PreGNN consists of three acceleration components: parallel graph generator (\emph{gGen}), unique random sampler (\emph{uSampler}), and near-interconnect gather (\emph{iGather}), each shorting the execution time of \texttt{Graph}, \texttt{Sample}, and \texttt{Reconst} over hardware.
gGen sorts a given edge array using radix information and generates the corresponding CSR matrix (vertex-centric) in a self-governing manner. uSampler picks given $n$ numbers of per-hop neighbor nodes associated with the target vertex (like the $k$-hop neighbor sampling). iGather creates the corresponding subgraph and sampled embedding table in a memory structure that the GPU can utilize.
We will first explain the essential algorithms and low-level hardware logic that our hardware acceleration components use and then describe the details of each automation in \textsection \ref{sec:implementation}.

\subsection{Designing Efficient Vertex/Edge Processing Algorithms}
While the main goal of our PreGNN's gGen and uSampler is different, the fundamental problem they have to address is similar, known as \emph{set partitioning}.
As shown in figure \ref{fig:over1}, set partitioning is the task of splitting a given set (array/digits) into two subsets, each having elements less than (or equal to) and greater than a given value, respectively.
The main function of gGen is sorting an array of edge information, consisting of pairs of  destination (\emph{dst}) VID and source (\emph{src}) VID (dst$\rightarrow$src).
gGen applies set partitioning based on radix information to effectively sort the vertices by a digit, repeated to perform a radix sort.
Similarly, when uSampler randomly selects neighbors of the target (dst vertex), it marks them as `0' (selected) or `1' and applies set partitioning using the mark as the key.
The sampled vertices then can be extracted from the front of the partitioned result.
Although implementing efficient set partitioning is essential for gGen and iSampler to accelerate GNN preprocessing, it is non-trivial to fully automate set partitioning over hardware, as evaluation of an element depends on the result of former elements, preventing parallel computation.
For example, set partitioning first needs to compare all elements of the array, which is bound by $\mathcal{O}(n)$, and then find out where each element should be located after the partitioning, which is more complicated than $\mathcal{O}(n)$.
For hardware acceleration, we provide two basic building blocks, \emph{scanner} and \emph{compactor}, which operate together to make the algorithm complexity of the set partitioning bounded by $\mathcal{O}(\log{}n)$.
Each building block employs an adder- or demux-based hardware tree whose depth is $\log{}n$, returning the output in a single cycle.
Overall, the scanner decides where each element should be relocated to after performing set partitioning, whereas the compactor shifts all elements based on the scanner's results in a cycle.

\subsection{Basic Building Blocks for Hardware Automation} \label{subsec:bb}
\noindent \textbf{Scanner.}
As shown in Figure \ref{fig:over2}, our scanner is combinational logic that simultaneously generates a ``second sequence'' of numbers (a, a+b, a+b+c, a+b+c+d) of an input sequence (a, b, c, d).
The challenge of this sequence generation over hardware is the dependency, as each element must accumulate all its left elements, forcing serial execution.
To parallelize the computation, our scanner employs a work-efficient scan algorithm by implementing a combinational adder tree in hardware.
Denoting the number of input elements as $n$, each level of the tree includes $n/2$ carry-save adders, each adding two values without a carry. All the carries are combined at the end by $n$ ripple-carry adders.

The insight is that the results of the right halves all include the total sum of the left half elements.
Thus, the scanner can individually compute the left half (a, b) and right half (c, d), then add the total of the left half (a+b) to all the right halves at the next level of the tree.
To double the number of elements, one additional step of adding the total of the left halves to all the right halves is sufficient.
As each level of the hardware tree reduces the computation by half, the second sequence can be generated in $\mathcal{O}(\log{}n)$, thereby completing in a single cycle.

\noindent \textbf{Compactor.}
Our compactor is designed toward moving each element of an input array to different positions by taking two input arrays, each containing content values (e.g., VIDs) and distance bitstreams. As shown in Figure \ref{fig:over3}, the compactor consists of $\log{}n$ levels of forwarding logic, each containing $n$ demuxes and $n$ OR gates where $n$ is the number of an input array.
Specifically, each element of the two input arrays (e.g., value and bitstream) is fed to the corresponding demux as a pair, and the demux decides which OR gate the input value should be forwarded.
Let us denote $i$ as the level index where the compactor's demuxes are located ($i \leq \log{}n$). Each level of the demuxes uses the $i^{th}$ bit of the input distance bitstream as the selection signal.
When the $i^{th}$ bit signal is `1', each demux forwards the value to the OR gate located  to the left by $2^{i-1}$. Otherwise, each demux directly forwards the input value to the right below OR gate.
Note that our compactor employs $\log{}n$ levels of forwarding logic, and each level's demuxes move the content values to the left by doubling the forwarding distance. Therefore, the compactor can simultaneously move all the content values to their designated position within a single cycle.

\section{Taking Preprocessing Off the Critical Path}
\label{sec:implementation}
\subsection{gGen: Accelerating Graph Generation}
The graph generation consists of two data processing phases: i) edge array sorting and ii) CSR composition.
Considering the benefits of the scanner/compactor's single cycle execution, gGen uses radix sort for edge array sorting.
As shown in Figure \ref{fig:design1}, gGen sorts the input array using set partitioning by collecting the least significant bits (LSBs) from all dst VIDs, applying them into the scanner, and repeating until the scanner finishes processing their most significant bits (MSBs).
All the VIDs having `0' should move to the left of an output array, while all the others (having `1') need to move to the right of the array.
For better understanding, let us call the former and later as \emph{left partitioning} and \emph{right partitioning}, respectively.
For the left partitioning, gGen needs to calculate how much each VID should be shifted, which can be indicated by the second sequence that the scanner generates.
For example, LSBs of the input array's five VIDs are [1, 0, 1, 1, 0], and thus, the scanner should turn out the corresponding second sequence, [1, 1, 2, 3, 3]. gGen simply knows that the $2_{nd}$ and $5_{th}$ VIDs should be shifted to the left by one and three times, respectively.
This shifting can be done by placing the second sequence in the compactor as input, collecting only the VIDs with bit `0' on the left.
Similarly, the right partitioning should move the remaining VIDs to the right, but our compactor can only move the input array's elements from right to left (\textsection \ref{subsec:bb}).
Note that the offset distance each VID (having `1') needs to move is the same as the number of its right-side VIDs having `0'. gGen thus first negates the input digits (`0'$\rightarrow$`1') and inverts their sequence. These negated and inverted digits are then fed into the scanner and compactor, such that all VIDs that have `1' (now `0' because of the negation) are collected to the left of the output.
To achieve the full sorted array, it inverts the output of right partitioning and concatenates with the output of left partitioning (over XOR). gGen iterates this process until MSBs of the VIDs are sorted ($b$ times for $b$-bit VIDs).
Lastly, gGen composes CSR by combining the sorted array's src VIDs with a pointer array.
Specifically, gGen makes indices and values of the pointer array be associated with dst VIDs and indicate corresponding offsets of the sorted array, respectively.
To this end, it counts the number of VIDs in each sorted output and keeps the count in the pointer array for all dst VIDs.
Using CSR, any GNN task can directly find out neighbors of a target through its VIDs (dst) without a graph traverse.

\begin{figure}[]
  \centering
  \vspace{-5pt}
  \includegraphics[width=1\linewidth]{images/design1.eps}
  \begin{subfigure}{1\linewidth}
    \caption{Set partitioning.}
    \label{fig:over1}
  \end{subfigure}
  \subcaptionbox{Scanner.\label{fig:over2}}[.4\linewidth]{}\unskip
  \subcaptionbox{Compactor.\label{fig:over3}}[.6\linewidth]{}\unskip
  \caption{Basic building blocks.}
  \vspace{-23pt}
\end{figure}

\subsection{uSampler: Hardware for Neighbor Sampling}
To automate the $k$-hop neighbor sampling over hardware, we can reduce the vertex selection problem to a bit-wise operation problem that randomly inserts $s$ numbers of `0' into a per-hop bitstream having the same length with the target neighbor list.
A simple solution, starting with a bitstream initialized as `1's and repeating to randomly select a bit and replace it with `0', cannot generate an appropriate bitstream because the same position can be regenerated, which fails to capture the natures of unique random sampling.
Instead, we leverage the scanner to implement unique random sampling over hardware. uSampler puts the bitstream as input to the scanner to first find the set of unselected positions. It then selects a position in the set and does iterate this scanning and selection process $s$ times (per-hop).
Specifically, the scanner's output is a monotonic increasing sequence with each rising point of the sequence at an unselected element, `1'.
uSampler can thus detect the rising points of the bitstream to select only among the unselected vertices in a random manner.
Once we achieve the bitstream having $s$ numbers of `0', uSample can extract the selected (`0') VIDs from the given array by set partitioning the array with the bitstream.

Figure \ref{fig:design2} shows how uSampler can sample neighbors (src VIDs) of a target node (dst VIDs) and compose the corresponding sampled array.
At the very beginning, uSampler prepares the bitstream whose number of digits is the same as the list's length and initializes them as `1's (unselected).
The input bitstream of this example is [1, 1, 0, 1], meaning that the $3_{rd}$ element was selected in the first iteration, and it is now the second iteration of our unique random sampling.
uSampler first inputs the bitstream to the scanner resulting in the corresponding second sequence, [1, 2, 2, 3].
Since uSampler knows how many elements remain by its iteration count, it can generate a random value ($v_{rsamp}$) less than the number of remaining elements.
Specifically, we employ a \emph{linear-feedback shift register} (LFSR \cite{wang1988linear}) to create pseudo\-random numbers and divide them with the remaining number of elements to obtain the remainder.
uSampler then compares each element of the second sequence with $v_{rsamp}$ and generates a third sequence, [0, 1, 1, 1]. It then logically shifts the third sequence to the right and deducts (XOR) the third sequence. This process effectively takes the derivative of the third sequence, which turns out to be [0, 1, 0, 0]. To unselect the second element from the original stream, it deducts the final result from the bitstream. The sampled bitstream in this iteration is again fed to the scanner as an input bitstream for the next step.
After generating the bitstream that has all sampling information, uSampler puts the neighbor list and bitstream as input to the compactor, respectively. It will then package the selected VIDs only at the left of the output array, which is used for a sampled subgraph of the original graph.

\begin{figure}[]
  \centering
  \vspace{-7pt}
  \begin{subfigure}{\linewidth}
      \centering
      \includegraphics[width=1\linewidth]{images/design2.eps}
      \begin{tabularx}{\textwidth}{
          p{\dimexpr.58\linewidth-2\tabcolsep-1.3333\arrayrulewidth}
          p{\dimexpr.42\linewidth-2\tabcolsep-1.3333\arrayrulewidth}
        }
          \vspace{-20pt} \caption{gGen (edge array sorting).} \label{fig:design1}
        & \vspace{-20pt} \caption{uSampler (per-hop).} \label{fig:design2}
      \end{tabularx}
  \end{subfigure}
  \vspace{-25pt}
  \caption{PreGNN's acceleration components.} \label{fig:design}
  \vspace{-18pt}
\end{figure}

\subsection{iGather: Handling Sampled Graph and Embeddings}
While uSampler can make an output list including only the selected VIDs, it is not yet formed as CSR that most GNNs use for per-hop traversing and aggregation. In addition, we need to lookup the original embedding table and generate the dataset the GPU will refer to, called sampled embeddings.
To this end, iGather composes CSR by adding up the array pointer to handle the selected VIDs' list (like gGen) and lookups the original embeddings to compose the corresponding sampled embedding table.
Note that the GPU requires awareness of the mapping between indices of the sampled embeddings and original embeddings to access the sampled embedding table.
To eliminate unnecessary overhead and data structures for the translation, iGather renumbers the sampled subgraph's VIDs based on the new indices of the sampled table.
GPUs can directly process the sampled subgraph/embeddings.
Since all the above tasks are related to memory operations, our iGather performs the tasks near interconnect.
Since all the nodes have the same size of feature vectors in GNNs, we can find out the physical memory addresses of the sampled embeddings by adding up the original table's base address with $VID*feature\_size$. iGather then simply appends (gathers) each feature vector to the end of the sampled embedding table (referring to its given base address). This gathering operation of iGather automatically generates all memory addresses and copies the data from the original to the sampled tables without any intervention. As most gathering tasks are associated with accessing the underlying memory sequentially, our near-interconnect processing can take all the data with massive bulk operations via bus bursts, thereby increasing data processing bandwidth.

\begin{figure}
  \centering
  \includegraphics[width=1\linewidth]{images/evaluation.eps}
  \begin{subfigure}{0\linewidth}
    \caption{}
    \label{fig:eval_overall}
  \end{subfigure}
  \begin{subfigure}{0\linewidth}
    \caption{}{}
    \label{tbl:eval_speedup}
  \end{subfigure}
  \begin{subfigure}{0\linewidth}
    \caption{}
    \label{fig:eval_energy}
  \end{subfigure}
  \caption{Evaluation.}
\end{figure}
