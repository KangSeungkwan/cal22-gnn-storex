We prototype the PreGNN coprocessor on 16$nm$ FPGA having 64GB DDR4 DRAM. The prototype (\texttt{PreGNN}) includes eight scanners and eight compactors that operate in parallel to support gGen and uSampler. On the other hand, iGather is connected to the system bus as a memory controller and opens the interface to gather graph datasets through memory mapped register (MMIO).
In addition, the prototype includes a simple, in-order RISC-V core, which handles the input and output of all the PreGNN acceleration modules.
We evaluate \texttt{PreGNN} on a testbed that employs a 1.8GHz Xeon CPU and high-end GPU (RTX 3090). For the performance analysis, we compare \texttt{PreGNN} with preprocessing performed by the host CPU, called \texttt{CpuGNN}.
As another coprocessor design, we also configured \texttt{ManyGNN} that integrates eight RISC-V cores in the same FPGA and parallelizes the preprocessing over their many threads. Note that all preprocessing tasks are handled by FPGA (\texttt{ManyGNN}/\texttt{PreGNN}) or CPU (\texttt{CpuGNN}), while GNN processing for all of them is performed in the GPU of our testbed. Except for \texttt{PreGNN}, all the preprocessing tasks are parallelized through DGL's state-of-the-art multithreading model \cite{wang2019dgl}. Specifically, we modified TensorFlow 2.4 for the multithreaded preprocessing model and GNN acceleration with GPU.
We select eight real-world graph workloads coming from \cite{yang2016revisiting}, and their important characteristics are explained in Table \ref{tbl:dataset}. Each dataset of the workloads exhibits different sizes of features and graphs, and we sorted them based on the feature size for better understanding.

\subsection{Overall Performance}
\label{subsec:eval_overall}
Figure \ref{fig:eval_overall} shows the inference service times of \texttt{ManyGNN} and \texttt{PreGNN}, which are normalized to those of \texttt{CpuGNN}.
In this analysis, we also separately show the fraction of GPU latency (\texttt{GPU}) for GNN inferences of each prototype that we tested.
\texttt{ManyGNN} can accelerate the preprocessing performance on average by 2.2$\times$ through its many threads across cores handling different parts of the input edge array and embedding table.
\texttt{PreGNN} shortens the end-to-end inference latency by 10.7$\times$ and 5.1$\times$, compared to \texttt{CpuGNN} and \texttt{ManyGNN}, respectively.
The main reason for this performance superiority is performing GNN processing tasks all over hardware, except for data copies from the host to the GPU and inference itself.
In our prototype, the scanners and compactors take only a single cycle to process 256 entries in parallel.
Thus, we can pipeline the input/output data accesses, scanners, and compactors to process large-scale features and graphs with a small number of hardware automation logic; we will analyze the speedup of \texttt{PreGNN} for each GNN preprocessing task and energy consumption shortly.
\texttt{PreGNN} successfully reduces the fraction of GNN preprocessing from 94\% to 45.2\% of the end-to-end inference time (including \texttt{GPU}).
If one only considers the latency of GNN preprocessing, \texttt{PreGNN} makes the preprocessing 23.3$\times$ and 10.2$\times$ shorter, compared to \texttt{CpuGNN} and \texttt{ManyGNN}, on average, respectively.

\subsection{Speedup Breakdown Analysis}
\noindent \textbf{Graph generation.} Figure \ref{fig:eval_speedup} shows how much \texttt{PreGNN} can accelerate each preprocessing task compared to \texttt{CpuGNN} and \texttt{ManyGNN}.
Note that the execution time of \texttt{CpuGNN} and \texttt{ManyGNN} for \texttt{Graph} increase as the number of edge array entries increases, due to the sorting algorithm being bound by $\mathcal{O}(n)$.
Although \texttt{ManyGNN} accelerates sorting by comparing the input VIDs in parallel, merging the sorted entries must be done serially, limiting performance.
Additionally, the fixed area budget limits the number of cores available, further limiting performance.
In contrast, gGen can complete the radix sort within a cycle, performing \texttt{Graph} in $\mathcal{O}(\log{}n)$ complexity, making it faster as the length of edge array increases.
Specifically, it shortens \texttt{Graph} of \texttt{CpuGNN} and \texttt{ManyGNN} by 22.3$\times$ and 10.4$\times$, on average, respectively.

\noindent \textbf{Neighbor sampling.} \texttt{Sample} is proportional to the number of nodes of the original graph.
Since \texttt{ManyGNN} distributes $k$-hop neighbor sampling across cores, it increases the sampling performance compared to \texttt{CpuGNN} by 4.15$\times$, on average.
However, parallel sampling also requires merging the sampled output list, introducing synchronization overhead for each step.
The merged output should also be formatted as CSR by the core-side software, which further limits the performance of \texttt{ManyGNN}.
In contrast, \texttt{PreGNN} shortens \texttt{Sample} of \texttt{CpuGNN} and \texttt{ManyGNN} by 11.4$\times$ and 3.3$\times$, respectively.
This is because \texttt{PreGNN} selects unique VIDs and extracts them in a single cycle.

\noindent \textbf{Data reconstruction.} \texttt{Reconst} is strongly related to the data size of sampled VIDs and embeddings.
With more threads involved in moving the VIDs and embeddings, \texttt{ManyGNN} can shorten \texttt{Reconst} of \texttt{CpuGNN} by 3.22$\times$, on average.
However, \texttt{ManyGNN}'s threads are still managed by core-side load/store instructions, resulting in frequent CPU intervention during data movement and preventing massive data transfers.
In contrast, iGather performs data copies near interconnect by calculating the source and destination addresses based on the VID information, thereby exhibiting 51.5$\times$ and 17.5$\times$ shorter average latency compared to \texttt{CpuGNN} and \texttt{ManyGNN}, respectively.
Additionally, iGather can handle bulk data transfer with memory burst operations, allowing 4kB granularity data loads/stores instead of 64B.

\subsection{Analysis for Energy Consumption}

Figure \ref{fig:eval_energy} analyzes the energy consumption behaviors of all the platforms that we tested.
\texttt{GPU}, due to its massive streaming processors and internal memory, dominates ($>$99.97\%) the total energy on \texttt{ManyGNN} and \texttt{PreGNN} systems, even though it takes only 34\% of the end-to-end latency, on average.
While \texttt{PreGNN} exhibits excellent energy consumption behaviors, because of \texttt{GPU}, the benefits (3.3$\times$ and 1.13$\times$ respectively better than \texttt{CpuGNN} and \texttt{ManyGNN}) are not as promising as the latency improvement (\textsection \ref{subsec:eval_overall}).
Note that when considering only the preprocessing task, \texttt{PreGNN} uses 2464.5$\times$ and 131.5$\times$ less energy than \texttt{CpuGNN} and \texttt{ManyGNN}, on average, respectively.
This is due to the full automation of GNN preprocessing tasks, eliminating inefficiencies while processing through general instructions.
